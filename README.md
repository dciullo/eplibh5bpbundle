EPLibH5bpBundle
=============

This bundle provides HTML5 Boilerplate functionality in a simple to deploy bundle.
It is derived from Jonathan Verrecchia's [Initializr](http://www.initializr.com/) 
Features include:

- H5BP 4.1.0
	- IE classes
	- plugins.js
	- CSS3
- Modernizer 2.6.2
- jQuery 1.9.1 (minified)
- Mobile-first Responsive design
- Google Chrome Frame and Analytics
- Favicon
- Apple Touch Icons
- Adobe Cross Domain

State
-----

While the provided feature set is certainly production ready, this Bundle is still under development.
As a result users must expect that the H5BP, Modernizer and jQuery in this version may have bugs beyond
my control and therefore problems can arise. I will update the bundle to the latest versions but not likely
on the release schedule of any of the included frameworks.

Documentation
-------------

The bulk of the documentation is stored in the `Resources/doc/index.md`
file in this bundle:

[Read the Documentation for master](https://bitbucket.org/dciullo/eplibh5bpbundle/src/master/Resources/doc/index.md)

Installation
------------

All the installation instructions are located in the [documentation](https://bitbucket.org/dciullo/eplibh5bpbundle/src/master/Resources/doc/index.md).

License
-------

This bundle is under the MIT license. See the complete license in the bundle:

    Resources/meta/LICENSE
