Getting Started With EPLibH5bpBundle
=====================================

## Installation

Installation is a quick (I promise!) 4 step process:

1. [Setting up the bundle](1-setting_up_the_bundle.md)

## Bundle Usage (v2.0.0 for Symfony 2.1.x/2.2.x)

EPHLib5bpBundle provides a simple way to get the benefits of HTML5 Boilerplate into your Symfony2 application.

1. Copy [base.html.twig](https://bitbucket.org/dciullo/eplibh5bpbundle/src/master/Resources/views/Default/base.html.twig) to your app/Resources/view/ directory
2. Edit line 25 of base.html.twig and replace @YOUR_BUNDLE with your bundle's appropriate name. Line looks like: '@YOUR_BUNDLE/Resources/public/css/main.css' 
3. Copy [main-copy-me-to-your-bundle.css](https://bitbucket.org/dciullo/eplibh5bpbundle/src/master/Resources/public/css/main-copy-me-to-your-bundle.css) to YOUR BUNDLE/Resources/css/ directory \('YOUR BUNDLE' is the your bundle with your DefaultController in it\)
4. Rename main-copy-me-to-your-bundle.css to main.css
5. Make all your overrides and changes in this main.css file and they should override all the defaults. The defaults are broken up into several files. All can be found [here](https://bitbucket.org/dciullo/eplibh5bpbundle/src/master/Resources/public/css/)






**Depricated**
=====================

## Bundle usage (v1.0.0 for Symfony 2.0.x)

EPHLib5bpBundle provides a simple way to get the benefits of HTML5 Boilerplate into your Symfony2 application.

1. Copy [base.html.twig](https://bitbucket.org/dciullo/eplibh5bpbundle/src/1.0.0/Resources/views/Default/base.html.twig) to your app/Resources/view/ directory
2. Edit line 25 of base.html.twig and replace @YOUR_BUNDLE with your bundle's appropriate name. Line looks like: '@YOUR_BUNDLE/Resources/public/css/style.css' 
3. Copy [style-copy-me-to-your-bundle.css](https://bitbucket.org/dciullo/eplibh5bpbundle/src/1.0.0/Resources/public/css/style-copy-me-to-your-bundle.css) to YOUR BUNDLE/Resources/css/ directory \('YOUR BUNDLE' is the your bundle with your DefaultController in it\)
4. Rename style-copy-me-to-your-bundle.css to style.css
5. Make all your overrides and changes in this style.css file and they should override all the defaults. The defaults are broken up into several files. All can be found [here](https://bitbucket.org/dciullo/eplibh5bpbundle/src/1.0.0/Resources/public/css/)
