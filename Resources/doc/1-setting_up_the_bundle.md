Setting up the bundle (v2.0.0 for Symfony 2.1.x/2.2.x)
=============================
### A) Modify your `composer.json` file

Ultimately, the EPLibH5bpBundle files should be downloaded to the
`vendor/Erosproject/Library/H5bpBundle` directory.

``` yml
    "autoload": {
        "psr-0": { 
        	... 
        	"Erosproject\\Library\\H5bpBundle": "vendor/"
        }
    },
    "require": {
        ...
        "Erosproject/Library/H5bpBundle": "2.*"
    },
    "name": "E.R.O.S. Project HTML5 Boilerplate with Initialzr Bundle",
    "description": "Provides HTML5 Boilerplate with Initialzr Mobile-first features in Symfony2",
    "repositories": [{
	    "type": "package",
	    "package": {
	    	"name": "Erosproject/Library/H5bpBundle",
	    	"version": "2.0.0",
	    	"source": {
	    		"url": "https://bitbucket.org/dciullo/eplibh5bpbundle.git",
	    		"type": "git",
	    		"reference": "2.0.0"
	    	}
    	}
	}],
```

### B) Run composer

``` bash
$ php composer.phar install
```

### C) Enable the bundle

Then, enable the bundle in the kernel:

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Erosproject\Library\H5bpBundle\EPLibH5bpBundle(),
    );
}
```

### D) Set up routing

Then, enable the bundle in the kernel:

``` php
<?php
// app/config/routing.yml
...
EPLibH5bpBundle:
    resource: "@EPLibH5bpBundle/Controller/"
    type:     annotation
    prefix:   /
```


**Depricated**

Setting up the bundle (v1.0.0 for Symfony 2.0.x)
=============================
### A) Download EPLibH5bpBundle

Ultimately, the EPLibH5bpBundle files should be downloaded to the
`vendor/bundles/Erosproject/Library/H5bpBundle` directory.

This can be done in several ways, depending on your preference. The first
method is the standard Symfony2 method.

**Using the vendors script**

Add the following lines in your `deps` file:

```
[EPLibH5bpBundle]
    git=https://bitbucket.org/dciullo/eplibh5bpbundle.git
    target=bundles/Erosproject/Library/H5bpBundle
```

Now, run the vendors script to download the bundle:

``` bash
$ php bin/vendors install
```

**Using submodules**

If you prefer instead to use git submodules, then run the following:

``` bash
$ git submodule add https://bitbucket.org/dciullo/eplibh5bpbundle.git vendor/bundles/Erosproject/Library/H5bpBundle
$ git submodule update --init
```

### B) Configure the Autoloader

Add the `Erosproject` namespace to your autoloader:

``` php
<?php
// app/autoload.php

$loader->registerNamespaces(array(
    // ...
    'Erosproject'       => __DIR__.'/../vendor/bundles',
));
```

### C) Enable the bundle

Then, enable the bundle in the kernel:

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Erosproject\Library\H5bpBundle\EPLibH5bpBundle(),
    );
}
```

### D) Set up routing

Then, enable the bundle in the kernel:

``` php
<?php
// app/config/routing.yml
...
EPLibH5bpBundle:
    resource: "@EPLibH5bpBundle/Controller/"
    type:     annotation
    prefix:   /
```

## That was it!
Check out the docs for information on how to use the bundle! [Return to the index.](index.md)
