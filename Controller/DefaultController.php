<?php

namespace Erosproject\Library\H5bpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/H5bp/{name}")
     * @Template()
     */
    public function indexAction($name)
    {
        return array('name' => 'gomer ' . $name);
    }
}
